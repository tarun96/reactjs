import React,{Component} from 'react';
import ReactDom from 'react-dom';
import Avatarlist from './Avatarlist';

class Avatar extends Component {
  constructor(){
    super();
    this.state = {
      name: "Mera naam"
    }
  }

  change(){
    this.setState({
      name: "Mera naam hai tarun"
    })
  }

  render(){
    const data = [
      {
        name: 'Tarun'
      },
      {
        name: 'Vinay'
      },
      {
        name: 'Khushbu'
      },
      {
        name: 'Lalit'
      }
    ]

    const list = data.map((card,i) =>{
      return <Avatarlist key={i} name = {data[i].name} />
    })

    return (
      <div>
        <h2 className = "tc">{this.state.name}</h2>
        {list}
        <button onClick = {() => this.change()} >Lets Go</button>
      </div>
    )
  }
}

export default Avatar;
