import React,{Component} from 'react';
import ReactDom from 'react-dom';

const Avatarlist = (props) => {
  return (
        <div className = "avatar_card ma4 bg-light-red dib pa4 grow shadow-4 tc">
          <img src = {`https://joeschmoe.io/api/v1/${props.name}`} alt = "Avatar" />
            <h1> {props.name} </h1>
            <p> {props.work} </p>
        </div>
  )
}

export default Avatarlist;
